.PHONY: clean install zip

install:
	composer install
	npm install
	npm run gulp

clean:
	rm -rf node_modules
	rm -rf css
	rm -rf js
	rm -rf thumbs/*
	rm -rf vendor

zip:
	git archive -o latest.zip HEAD
