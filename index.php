<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(E_ALL ^ E_NOTICE);

function thumbnailImage($filename, $width, $height, $dirname)
{
    $source = sprintf('/usr/%s/%s', $dirname, $filename);
    $target = sprintf('/thumbs/%s/%dx%d_%s', $dirname, $width, $height, $filename);

    if (is_dir(__DIR__ . dirname($target)) === false) {
        mkdir(__DIR__ . dirname($target), 0755, true);
    }

    if (is_file(__DIR__ . $target)) {
        return $target;
    }

    $image = new \Kisphp\ImageResizer();

    // load original image file
    $image->load(__DIR__ . $source);


    // set where thumbnail will be saved (optional)
    $image->setTarget(__DIR__ . $target);

    // resize image to a 300px width and dynamic height by aspect ratio
    $image->resize($width, $height, true);

    $image->save();

    return $target;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<?php

$width = 350;
$height = 300;

?>

<div class="container">
    <div class="row">
        <div class="col-sm-4 col-md-4">
            <div class="thumbnail">
                <img src="<?php echo thumbnailImage('image-1.jpeg', $width, $height, 'aaa'); ?>" alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4">
            <div class="thumbnail">
                <img src="<?php echo thumbnailImage('image-2.jpeg', $width, $height, 'aaa'); ?>" alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4">
            <div class="thumbnail">
                <img src="<?php echo thumbnailImage('image-3.jpeg', $width, $height, 'aaa'); ?>" alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/js/app.js"></script>
</body>
</html>
