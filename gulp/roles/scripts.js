let gulp = require('gulp');
let concat = require('gulp-concat');

let config = {
    sources: [
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'gulp/assets/scripts/index.js'
    ],
    target: {
        directory: './js/',
        filename: 'app.js'
    }
};

gulp.task('js', () => {
    "use strict";
    return gulp.src(config.sources)
        .pipe(concat(config.target.filename))
        .pipe(gulp.dest(config.target.directory));
});

gulp.task('watch:js', () => {
    "use strict";

    gulp.watch('gulp/assets/scripts/**/*.js', ['js']);
});

let GR = require('kisphp-gulp-commander');

GR.addTask('js'); // here you register the main scss task
GR.addWatch('watch:js'); // here you register the watch task