let gulp = require('gulp');
let concat = require('gulp-concat');
let scss = require('gulp-sass');

let config = {
    sources: [
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'gulp/assets/scss/index.scss'
    ],
    target: {
        directory: './css/',
        filename: 'style.css'
    }
};

gulp.task('scss', () => {
    "use strict";
    return gulp.src(config.sources)
        .pipe(scss())
        .pipe(concat(config.target.filename))
        .pipe(gulp.dest(config.target.directory));
});

gulp.task('watch:scss', () => {
    "use strict";

    gulp.watch('gulp/assets/scss/**/*.scss', ['scss']);
});

let GR = require('kisphp-gulp-commander');

GR.addTask('scss'); // here you register the main scss task
GR.addWatch('watch:scss'); // here you register the watch task